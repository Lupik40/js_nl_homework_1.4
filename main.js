/* 
    При выполнении домашнего задания используйте только те конструкции,
    которые есть в уроке.

    TASK 0:

    Напишите функцию sum(c,t), которая возвращает результат суммы c,t

*/

function sum(c, t) {
    return c + t;
}

const sumResult = sum(10, 20);

/* 
    TASK 1:

    Напишите функцию min(a,b), которая возвращает меньшее из чисел a и b

*/

function min(a, b) {
    if (a > b) {
        return b;
    } else {
        return a;
    }
}

const minResult = min(1, 2);

/* 
    TASK 2:

    Напишите функцию pow(x,n), которая возвращает x, в степени n

*/

function pow(x, n) {
    return x ** n;
}

const powResult = pow(2, 3);

/* 
    TASK 3:

    Напишите функцию, которая принимает число,
    и возвращает строку "четное" или "нечетное".

*/

function oddOrEven(num) {
    if (num % 2 == 0) {
        return 'четное';
    } else {
        return 'нечетное';
    }
}

const oddOrEvenResult = oddOrEven(3);

/* 
    TASK 4:

    Напишите функцию getColor(23, 100, 134), которая будет принимать три аргумента 
    и возвращать строку вида "rgb(23,100,134)". 
    Если какой-либо из аргументов не задан: например, третий:
    мы вызываем функцию getColor(23,100), в таком случае мы должны
    получить строку "rgb(23,100,0)"

*/

function getColor(r, g, b) {
    r = typeof r !== 'undefined' ?  r : 0;
    g = typeof g !== 'undefined' ?  g : 0;
    b = typeof b !== 'undefined' ?  b : 0;
    return "rgb(" + r + ',' + g + ',' + b + ')';
}

const getColorResult = getColor(255, 255, 255);
const getColorResult2 = getColor(255);

/* 
    TASK 5:

    Напишите 2 функции:

    Первая функция squareNumber(num) должна принимать число, и возвращать квадрат этого числа

    Вторая функция запрашивает у пользователя число от 18 до 50.
    Если пользователь ввел НЕ число, то сделайте ему одно замечание,
    если число, то вызовете функцию squareNumber передав в нее это самое число.
    Необходимо вывести результат пользователю (Либо замечание, либо квадрат числа).  

*/

function squareNumber(num) {
    return num * num;
}

function getNumber() {
    let userNum = prompt('Введите число от 18 до 50');
    if (isNaN(userNum)) {
        alert('Попросили же число...')
    } else {
        if (userNum >= 18 && userNum <= 50) {
            alert(squareNumber(userNum));
        } else {
            alert('Число не в диапазоне')
        }
    }
}

getNumber();